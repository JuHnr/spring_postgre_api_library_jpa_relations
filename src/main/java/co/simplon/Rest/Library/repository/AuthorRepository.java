package co.simplon.Rest.Library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.Rest.Library.entity.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>{


    
}
