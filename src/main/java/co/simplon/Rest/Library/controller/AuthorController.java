package co.simplon.Rest.Library.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.Rest.Library.entity.Author;
import co.simplon.Rest.Library.service.AuthorService;
import jakarta.persistence.EntityNotFoundException;

@RestController
public class AuthorController {

    // Injection de dépendance par constructeur

    private AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    // Endpoints du CRUD

    @GetMapping("/authors")
    public Iterable<Author> getAuthors() {
        return authorService.getAuthors();
    }

    @GetMapping("/authors/{id}")
    public ResponseEntity<Author> getAuthorById(@PathVariable("id") Long id) {
        try {
            // renvoie l'auteur et le statut de la requête (OK si trouvé, NOT_FOUND sinon)
            return new ResponseEntity<Author>(authorService.getAuthorById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<Author>(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/authors")
    public Author createAuthor(@RequestBody Author author) {
        return authorService.createAuthor(author);
    }

    @PutMapping("/authors/{id}")
    public Author updateAuthor(@PathVariable("id") Long id, @RequestBody Author author) {
        return authorService.updateAuthor(id, author);
    }

    @DeleteMapping("/authors/{id}")
    public String deleteAuthor(@PathVariable("id") Long id) {
        authorService.deleteAuthor(id);
        return "The author has been deleted successfully";
    }
}
