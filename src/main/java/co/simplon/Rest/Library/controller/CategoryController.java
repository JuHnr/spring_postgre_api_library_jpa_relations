package co.simplon.Rest.Library.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.Rest.Library.entity.Category;
import co.simplon.Rest.Library.service.CategoryService;
import jakarta.persistence.EntityNotFoundException;

@RestController
public class CategoryController {

    // Injection de dépendance par constructeur

    private CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    // Endpoints du CRUD

    @GetMapping("/categories")
    public Iterable<Category> getCategories() {
        return categoryService.getCategories();
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<Category> getCategoryById(@PathVariable("id") Long id) {
        try {
            // renvoie la catégorie et le statut de la requête (OK si trouvé, NOT_FOUND sinon)
            return new ResponseEntity<Category>(categoryService.getCategoryById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/categories")
    public Category createCategory(@RequestBody Category category) {
        return categoryService.createCategory(category);
    }

    @PutMapping("/categories/{id}")
    public Category updateCategory(@PathVariable("id") Long id, @RequestBody Category category) {
        return categoryService.updateCategory(id, category);
    }

    @DeleteMapping("/categories/{id}")
    public String deleteCategory(@PathVariable("id") Long id) {
        categoryService.deleteCategory(id);
        return "The category has been deleted successfully";
    }
}
