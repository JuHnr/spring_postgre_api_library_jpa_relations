package co.simplon.Rest.Library.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.Rest.Library.entity.Book;
import co.simplon.Rest.Library.service.BookService;
import jakarta.persistence.EntityNotFoundException;

@RestController
public class BookController {

    // Injection de dépendance par constructeur

    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    // Endpoints du CRUD

    @GetMapping("/books")
    public Iterable<Book> getBooks() {
        return bookService.getBooks();
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<Book> getBookById(@PathVariable("id") Long id) {
        try {
            // renvoie le livre et le statut de la requête (OK si trouvé, NOT_FOUND sinon)
            return new ResponseEntity<Book>(bookService.getBookById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/books/title")
    public Book getBookByTitle(@RequestParam String title) {
        return bookService.getBookByTitle(title);
    }

    @PostMapping("/books")
    public Book createBook(@RequestBody Book book) {
        return bookService.createBook(book);
    }

    @PutMapping("/books/{id}")
    public Book updateBook(@PathVariable("id") Long id, @RequestBody Book book) {
        return bookService.updateBook(id, book);
    }

    @DeleteMapping("/books/{id}")
    public String deleteBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
        return "The book has been deleted successfully";
    }
}
