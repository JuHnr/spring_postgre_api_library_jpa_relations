package co.simplon.Rest.Library.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import co.simplon.Rest.Library.entity.Category;
import co.simplon.Rest.Library.repository.CategoryRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class CategoryService {

    // Injection de dépendance par constructeur

    private CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    // Méthodes

    public Iterable<Category> getCategories() {
        return categoryRepository.findAll();
    }

    public Category getCategoryById(Long id) {
        // on cherche la catégorie par son id
        Optional<Category> optionalCategory = categoryRepository.findById(id);

        // si elle est trouvé alors on retourne la catégorie sinon on lève une exception
        if (optionalCategory.isPresent()) {
            return optionalCategory.get();
        } else {
            throw new EntityNotFoundException("The category doesn't exist.");
        }
    }

    public Category createCategory(Category category) {
        return categoryRepository.save(category);
    }

    public Category updateCategory(Long id, Category category) {
        // cherche par id et récupère la catégorie
        Category currentCategory = categoryRepository.findById(id).get();
        // remplace les données par les nouvelles
        currentCategory.setName(category.getName());
        // sauvegarde et retourne la catégorie mise à jour
        return categoryRepository.save(currentCategory);
    }

    public void deleteCategory(Long id) {
        categoryRepository.deleteById(id);
    }
}
