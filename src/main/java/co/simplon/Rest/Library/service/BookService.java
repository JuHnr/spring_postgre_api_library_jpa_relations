package co.simplon.Rest.Library.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import co.simplon.Rest.Library.entity.Book;
import co.simplon.Rest.Library.repository.BookRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class BookService {

    // Injection de dépendance par constructeur

    private BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    // Chaque méthode a pour seul objectif d'appeler une méthode du repository

    public Iterable<Book> getBooks() {
        return bookRepository.findAll();
    }

    public Book getBookById(Long id) {
        // on cherche le livre par son id
        Optional<Book> optionalBook = bookRepository.findById(id);

        // s'il est trouvé alors on retourne le livre sinon on lève une exception
        if (optionalBook.isPresent()) {
            return optionalBook.get();
        } else {
            throw new EntityNotFoundException("The book doesn't exist.");
        }
    }

    public Book getBookByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    public Book updateBook(Long id, Book book) {
        // cherche par id et récupère le livre
        Book currentBook = bookRepository.findById(id).get();
        // remplace les données par les nouvelles
        currentBook.setTitle(book.getTitle());
        currentBook.setDescription(book.getDescription());
        currentBook.setAvailable(book.isAvailable());
        // sauvegarde et retourne le livre mis à jour
        return bookRepository.save(currentBook);
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }
}
