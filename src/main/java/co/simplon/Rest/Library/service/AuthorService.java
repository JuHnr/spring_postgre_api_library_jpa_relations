package co.simplon.Rest.Library.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import co.simplon.Rest.Library.entity.Author;
import co.simplon.Rest.Library.repository.AuthorRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class AuthorService {
    
    // Injection de dépendance par constructeur

    private AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    // Méthodes

    public Iterable<Author> getAuthors() {
        return authorRepository.findAll();
    }

    public Author getAuthorById(Long id) {
        // on cherche l'auteur par son id
        Optional<Author> optionalAuthor = authorRepository.findById(id);

        // s'il est trouvé alors on retourne l'auteur sinon on lève une exception
        if (optionalAuthor.isPresent()) {
            return optionalAuthor.get();
        } else {
            throw new EntityNotFoundException("The author doesn't exist.");
        }
    }

    public Author createAuthor(Author author) {
        return authorRepository.save(author);
    }

    public Author updateAuthor(Long id, Author author) {
        // cherche par id et récupère l'auteur
        Author currentAuthor = authorRepository.findById(id).get();
        // remplace les données par les nouvelles
        currentAuthor.setFirstName(author.getFirstName());
        currentAuthor.setLastName(author.getLastName());

        // sauvegarde et retourne l'auteur mis à jour
        return authorRepository.save(currentAuthor);
    }

    public void deleteAuthor(Long id) {
        authorRepository.deleteById(id);
    }
}
